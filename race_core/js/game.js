var game = {
    data : {
        firstPageId : 584,
        quantumPageId : 319,
        isTestMode: false,
        isMobile: false,
        page : {},
        actions : [],
        disableActions : [],
        events : [],
        onlyEvents : false, 
        notices : []
    },
    initialize : function () {
        AR.getDb();
    },
    newGame : function (isQuantum) {      
        $('body').removeClass('preload');
        $('.tablet').addClass('active');
        $('.gameUI.text').addClass('active');
        $('.gameUI').each(function (index) {
            new SimpleBar($('.gameUI')[index]);
        });
        player.clearData();
        player.initialize();
        if(isQuantum) {
            this.data.page = AR.getPageById(this.data.quantumPageId);
        } else {
            this.data.page = AR.getPageById(this.data.firstPageId);
        }
        this.show();
        interface.show();
    },
    continueGame : function () {
        $('body').removeClass('preload');
        if (player.checkSave()) {
            $('.tablet').addClass('active');
            $('.gameUI.text').addClass('active');
            $('.gameUI').each(function (index) {
                new SimpleBar($('.gameUI')[index]);
            });
            let page = AR.getPageById(player.getPage());
            this.data.page = page;
            this.show();
            interface.show();
        }
    },
    move : function (actionId) {
        this.data.events = [];
        this.clearNotice();
        let action = AR.getAction(actionId);
        trigger.execute(action.callback);
        this.data.page = AR.getPageById(action.to_id);
        trigger.executeTriggersOnes(this.data.page.id);
        player.setPage(this.data.page.id);
        trigger.visitPage(this.data.page.id);
        player.addCounter("Первый счётчик", 1);
        this.show();
    },
    show : function () {
        view.showPage();
        view.showStats();
        trigger.executeTriggersMultiple(this.data.page.id);
        this.getActions();
        view.showActions();
        visual.call();
        music.call();
        view.showNotices();
        view.showEffects();
        player.saveData();
        quantum.show();
        game.data.onlyEvents = false;
    },
    loose : function (){
        this.data.onlyEvents = false;
        player.setPage(52);//страница с текстом поражения
        player.saveData();
        this.continueGame();
    },
    addNotice : function (text){
        this.data.notices.push(text);
    },
    clearNotice : function (){
        this.data.notices = [];
    },
    getActions : function(){
        game.data.actions = [];
        game.data.disableActions = [];
        let actions = AR.getActions(this.data.page.id);
        let sortedActions = {};
        $.each(actions, function(key, action){
            if (sortedActions[action.index] === undefined){
                sortedActions[action.index] = [];
            }
            let condition = (!action.condition || trigger.execute(action.condition));
            if (condition !== null) {
                if (condition){
                    sortedActions[action.index].push(action);
                }else{
                    game.data.disableActions.unshift(action);
                }
            }
        });
        for (key in sortedActions){
            app.shuffleArray(sortedActions[key]);
            $.each(sortedActions[key], function(key, value){
                game.data.actions.unshift(value);
            });
        }
    }
};
