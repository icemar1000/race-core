var card = {
    suit : "",
    text : "",
    SUIT_HEARTS : "hearts",
    SUIT_CLUBS : "clubs",
    SUIT_SPADES : "spades",
    SUIT_DIAMONDS : "diamonds",
    SUIT_JOKER : "joker",
    PLACE_DECK :"deck",
    PLACE_HAND :"hand",
    PLACE_DISCARD :"discard",
    PLACE_JOKER :"joker_place",
    
    getSuits : function (){
        return [
            this.SUIT_HEARTS,
            this.SUIT_CLUBS,
            this.SUIT_DIAMONDS,
            this.SUIT_SPADES
        ];
    },
    getValues : function (){
        return {
            "ten" : 0,
            "jack" : 1,
            "queen" : 2,
            "king" : 3,
            "ace" : 4,
        };
    },
    getValue : function (title){
        return this.getValues()[title];
    },
    getRandSuit : function(){
        return this.getSuits()[app.rand(0,4)];
    },
    getBySuit : function(suit, place){
        let deck = player.getCards(place);
        let result = {};
        $.each(deck, function(key, value){
            if (value.suit==suit){
                result[key] = value;
            }
        });
        return result;
    },
    getValuesLabel : function(){
        return {
            0 : "Десятка",
            1 : "Валет",
            2 : "Дама",
            3 : "Король",
            4 : "Туз",
            5 : ""
            
        };
    },
    getValueLabel : function(value){
        return this.getValuesLabel()[value];
    },
    getSuitsLabel : function(){
        return {
            'hearts' : "червей",
            'spades' : "пик",
            'diamonds' : "бубен",
            'clubs' : "треф",
            'joker' : "Джокер"
        };
    },
    getSuitLabel : function(suit){
        return this.getSuitsLabel()[suit];
    },
    getSuitsLabelForDraw : function(){
        return {
            'hearts' : "червовую",
            'spades' : "пиковую",
            'diamonds' : "бубновую",
            'clubs' : "трефовую",
            'joker' : "Джокер"
        };
    },
    getSuitLabelForDraw : function(suit){
        return this.getSuitsLabelForDraw()[suit];
    }
};
