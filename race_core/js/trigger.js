var trigger = {
    prepareCallback : function (callback){
        callback = callback.replace(/\$/g, "trigger.");
        callback = callback.replace(/ and /g, " && ");
        callback = callback.replace(/ or /g, " || ");
        callback = callback.replace(/\?:/g, " ?true: ");
        callback = callback.replace(/max/g, "Math.max");
        return callback;
    },
    executeTriggersOnes: function(id){
        $.each(AR.getTriggersByPageOnes(id), function (key, value){
            if (!value.condition || trigger.execute(value.condition)){
                trigger.execute(value.callback);
            }
        });
    },
    executeTriggersMultiple: function(id){
        $.each(AR.getTriggersByPageMultiple(id), function (key, value){
            if (!value.condition || trigger.execute(value.condition)){
                trigger.execute(value.callback);
            }
        });
    },
    createEvent : function (text, event){
        game.data.events.push({
            text : text,
            event : event
        });
    },
    execute : function(callback){
        game.data.events = [];
        if (callback != "time(0)"){
            try {
                console.log(this.prepareCallback(callback));
                return eval(this.prepareCallback(callback));
            } catch(err){
                console.log(err);
            }
        }
    },
    // api
    changeMoney : function(value){
        player.changeMoney(value);
    },
    addCounter : function(title, value){
        player.addCounter(title,value);
    },
    buy : function (title, value, trigger = ""){
        let object = AR.getObjectByTitle(title);
        if (value <= player.game.money){
            this.createEvent("Купить "+ object.text + " за " + value + " кр.", 'player.buyItem("' + object.title + '",' + value + ');');
        }
        return true;
    },
    discardItem : function (){
        let items = player.getItems();
        game.data.onlyEvents = true;
        $.each(items, function(key, item){
            trigger.createEvent("Выбросить "+ item.text , 'player.removeObject('+ key +');')
        });
    },
    giveObject : function (title){
        let object = AR.getObjectByTitle(title);
        player.addObject(object);
    },
    drawCard : function (suit = false){
        let cards = {};
        if (!suit){
            cards = player.getCards(card.PLACE_DECK);
            console.log(cards);
        }else{
            cards = card.getBySuit(suit, card.PLACE_DECK);
        }
        if (Object.keys(cards).length != 0){
            let keys = Object.keys(cards);
            let choose = app.rand(0, keys.length - 1);
            player.drawCard(keys[choose]);
        }else{
            //если нет карты выбранной масти, то выдаем карту другой масти
            this.drawCard(card.getRandSuit());
        }
    },
    drawJoker : function () {
        joker = card.getBySuit(card.SUIT_JOKER, card.PLACE_JOKER);
        $.each(joker, function(key, value){
            player.drawCard(key);
        });
    },
    discardCard : function (suit = false, minValue = false, quantity = 1, drawAfter = false){
        let available = this.checkCard(suit, minValue);
        if ((available && Object.keys(available).length!=0) || drawAfter){
            $.each(available, function(key, value){
                let event = "";
                let text = "Сбросить " + card.getValueLabel(value.value) +" "+card.getSuitLabel(value.suit);
                if (quantity!=1){
                    event = "player.discardCard(" + key + ");" + "trigger.discardCard(false, false," + (parseInt(quantity)-1) + ");";
                }else{
                    event = "player.discardCard(" + key + ");";
                }
                if (drawAfter){
                    event += "trigger.drawAfter(2);";
                    text += " и взять две карты."
                }
                trigger.createEvent(text, event);
            });
        }else{
            trigger.createEvent("Поражение", "game.loose();");
        }
        if (!drawAfter){
            game.data.onlyEvents = true;
        }
    },
    drawAfter : function (quantity){
        let suits = card.getSuits();
        $.each(suits, function(key,suit){
            cards = card.getBySuit(suit, card.PLACE_DECK);
            if (Object.keys(cards).length!=0){
                let event = "";
                if (quantity!=1){
                    event = "trigger.drawCard(\"" + suit + "\");" + "trigger.drawAfter(" + (parseInt(quantity)-1) + ");";
                }else{
                    event = "trigger.drawCard(\"" + suit + "\");"
                }
                trigger.createEvent("Взять " + card.getSuitLabelForDraw(suit), event)
            }
        });
    },
    getValue : function (titleItem){
        let id = player.getGameObjectIdByTitle(titleItem);
        if(id) {
            return player.gameObjects[id].value;
        }
        return 0;
    },
    checkValue : function(titleItem, minValue) {
        if (titleItem == "money"){
            return player.game.money >= minValue;
        }
        return this.getValue(titleItem) >= minValue;
    },
    checkObject : function(titleItem){
        return player.getGameObjectIdByTitle(titleItem);
    },
    checkCard : function (suit, minValue){
        let cards = [];
        if (suit){
            cards = card.getBySuit(suit, card.PLACE_HAND);
        }else{
            cards = player.getCards(card.PLACE_HAND);
        }
        let available = {};
        $.each(cards, function(key, value){
            if (!minValue || parseInt(value.value) >= card.getValue(minValue)){
                available[key] = value;
            }
        })
        if (Object.keys(available).length!=0){
            return available;
        }
        return false;
    },
    dontShow : function (eval){
        let result = this.execute(eval);
        if (result){
            return result;
        }
        return null;
    },
    checkItems : function (){
        let items = player.getItems();
        if (Object.keys(items).length!=0){
            return true;
        }
        return false;
    },
    visitPage :function (pageId){
        player.visitPage(pageId);
    },
    checkVisitedPages : function (pageIds){
        let result = true;
        $.each(pageIds, function(key, pageId){
            if (!trigger.checkVisitedPage(pageId)){
                result = false;
            }
        });
        return result;
    },
    checkVisitedPage : function (pageId){
        return (player.game.visitedPages.indexOf(pageId.toString()) != -1);
    },
    clearQuantumPages : function (){
        let pageIds = [548,553,558,566,563];
        player.game.visitedPages = player.game.visitedPages.filter(function(pageId) {
            if (pageIds.indexOf(pageId) == -1) {
                return pageId;
            }
        });
    },
    dropObject : function (titleItem){
        let object = AR.getObjectByTitle(titleItem);
        player.removeObject(object.id);
    },
    dropAll : function (){
        player.clearData();
        player.initialize();
        game.show();    
    },
    drawCardFromDiscard : function () {
        let cards = player.getCards(card.PLACE_DISCARD);
        $.each(cards, function(key, value){
            trigger.createEvent("Взять " + card.getValueLabel(value.value) +" "+card.getSuitLabel(value.suit), "player.drawCard(" + key + ");");
        });
        game.data.onlyEvents = true;
    },
    getRandomKeyword : function () {
        let keywords = AR.getObjectsKeywords();
        keywords = keywords.filter(function (keyword){
            if (!player.getGameObjectIdByTitle(keyword.title)){
                return keyword;
            }
        });
        let items = [];
        for (let i=0;i<3;i++){
            let key = Math.floor(Math.random()*keywords.length);
            items.push(keywords[key]);
            keywords.splice(key, 1);
        }
        $.each(items, function(key, value){
            trigger.createEvent(value.text, "trigger.giveObject(\""+ value.title +"\");")
        });
        game.data.onlyEvents = true;
    },
    sellCard : function (price){
        let cards = player.getCards(card.PLACE_HAND);
        $.each(cards, function(key, value){
            trigger.createEvent("Продать " + card.getValueLabel(value.value) +" "+card.getSuitLabel(value.suit), "player.discardCard(" + key + ");player.changeMoney(" + price + ")");
        });
    },
    sellObject : function (price){
        let objects = player.getItems();
        $.each(objects, function(key, value){
            trigger.createEvent("Продать " + value.text, "player.removeObject(" + key + ");player.changeMoney(" + price + ")");
        });
    },
    randMove : function (showActions){
        game.data.onlyEvents = true;
        let allActions = AR.getActions(player.game.pageId);
        allActions = allActions.filter(function(action){
            return player.game.visitedPages.indexOf(parseInt(action.to_id)) == -1
                        && showActions.indexOf(action.id) == -1 
                        && (!action.condition || trigger.execute(action.condition));
        });
        if (allActions.length != 0){
            let key = Math.floor(Math.random() * allActions.length);
            let choosedAction = allActions[key];
            trigger.createEvent(choosedAction.text, "game.move(" + choosedAction.id + ")");
        }
        $.each(showActions, function(key, actionId){
            let action = AR.getAction(actionId);
            trigger.createEvent(action.text, "game.move(" + action.id + ")");
        });
    },
    newGame : function () {
        game.newGame();
    },
    closeGame : function(actionId) {
        let action = AR.getAction(actionId);
        game.data.page = AR.getPageById(action.to_id);
        trigger.visitPage(game.data.page.id);
        player.addCounter("Первый счётчик", 1);
        player.saveData();
        app.close();
    },
    playSound : function(name, delay = 0){
        let src = "sound/" + name + '.mp3';
        let sound = new Howl({
            src: [src],
        });
        setTimeout(function(){
            sound.play();
        }, delay);
    }
};