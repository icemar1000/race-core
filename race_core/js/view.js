var view = {
    viewMarkers : [
        "Маркер. Буби",
        "Маркер. Пики",
        "Маркер. Трефы",
        "Маркер. Черви"
    ],
    initialize : function () {
        if (game.data.isTestMode) {
            $("#testBar").show();
        }
        let setBaseFontSize = function () {
            if (window.innerHeight > window.innerWidth) {
                document.body.style.fontSize = (window.innerHeight / 50) + 'px';
            } else {
                document.body.style.fontSize = (window.innerWidth / 50) + 'px';
            }
        };
        setBaseFontSize();
        window.onresize = function () {
            setBaseFontSize();
        };
        if (game.data.isMobile) {
            document.addEventListener("deviceready", function () {
                this.showMenu();
            }, false);
        } else {
            this.showMenu();
        }
    },
    showMenu : function () {
        //this.createMenu();
        $("#menu").show();
        $('.tablet').removeClass('active');
        if (player.checkSave()) {
            $('.loadGame').removeClass('disable').addClass('active');
        } else {
            $('.loadGame').addClass('disable').removeClass('active');
        }
    },
    showPage : function () {
        let pageText = (game.data.isTestMode) ? (game.data.page.id + ": " + game.data.page.text) : game.data.page.text;
        $("#page").html(pageText);
        //$("#page").show();
        $("#menu").hide();
        document.querySelector('.gameUI.text').SimpleBar.getScrollElement().scrollTop = 0;
    },
    showNotices : function () {
        $(".gameNotices").html("");
        $.each(game.data.notices, function (key, notice) {
            $(".gameNotices")
                .append('<div class="notice">'
                            + '<svg width="1.25em" height="1.25em" class="octicon octicon-info" viewBox="0 0 14 16" version="1.1" aria-hidden="true">'
                            +     '<path fill="#ffffff" fill-rule="evenodd" d="M6.3 5.69a.942.942 0 0 1-.28-.7c0-.28.09-.52.28-.7.19-.18.42-.28.7-.28.28 0 .52.09.7.28.18.19.28.42.28.7 0 .28-.09.52-.28.7a1 1 0 0 1-.7.3c-.28 0-.52-.11-.7-.3zM8 7.99c-.02-.25-.11-.48-.31-.69-.2-.19-.42-.3-.69-.31H6c-.27.02-.48.13-.69.31-.2.2-.3.44-.31.69h1v3c.02.27.11.5.31.69.2.2.42.31.69.31h1c.27 0 .48-.11.69-.31.2-.19.3-.42.31-.69H8V7.98v.01zM7 2.3c-3.14 0-5.7 2.54-5.7 5.68 0 3.14 2.56 5.7 5.7 5.7s5.7-2.55 5.7-5.7c0-3.15-2.56-5.69-5.7-5.69v.01zM7 .98c3.86 0 7 3.14 7 7s-3.14 7-7 7-7-3.12-7-7 3.14-7 7-7z"></path>'
                            + '</svg>'
                            + notice
                        + '</div>');
        });
    },
    showActions : function () {
        $("#actions").html("");
        if (game.data.events.length != 0) {
            $.each(game.data.events, function (key, event) {
                $("#actions").append("<li><div><a href='#' class='link action manual' data-event='" + event.event + "'>" + event.text + "</a></div></li>");
            });
        }
        if (!game.data.onlyEvents) {
            $.each(game.data.actions, function (key, action) {
                    let actionText = (game.data.isTestMode) ? (action.to_id + ": " + action.text) : (action.text);
                    $("#actions").append("<li><div><a data-key ='" + key + "' href='#' class='link action' data-id='" + action.id + "'>" + actionText + "</a></div></li>");
            });
            $.each(game.data.disableActions, function (key, action) {
                    let actionText = (game.data.isTestMode) ? (action.to_id + ": " + action.text) : (action.text);
                    $("#actions").append("<li><div><a data-key ='" + key + "' href='#' class='disable link action' data-id='" + action.id + "'>" + actionText + "</a></div></li>");
            });
        }
    },
    createMenu : function () {
            if (player.checkSave()) {
                $("#menu").append("<a href='#' class='menu-link' onclick='game.continueGame()'>Продолжить</a>");
            }
            $("#menu").append("<a href='#' class='menu-link' onclick='game.newGame()'>Новая игра</a>");
            $("#menu").append("<a href='#' class='menu-link' onclick='app.close()'>Выйти</a>");
    },
    showStats : function () {
        let items = player.getItems();
        let cards = player.getCards(card.PLACE_HAND);
        let upgrades = player.getUpgrades();
        let markers = player.getMarkers();
        let keywords = player.getKeywords();

        $(".playerStats_container").html("");

        $("#playerStats_money .playerStats_container").html(player.game.money + ' CR');

        $.each(markers, function (key, value) {
            $("#playerStats_top div[data-suit='" + value.text + "'] .playerStats_container").html(
                '<div class="progress">'
                + '<div class="progress-bar" role="progressbar" aria-valuenow="'
                + value.value
                + '" aria-valuemin="0" aria-valuemax="20" style="width: '
                + value.value * 5
                + '%">'
                + '</div>'
                + '</div>'
            );
        });

        if (Object.keys(cards).length > 0) {
            $("#playerStats_cards .playerStats_container").append('<ul></ul>');
            $.each(cards, function (key, value) {
                $("#playerStats_cards .playerStats_container ul").append(
                    "<li>"
                    + card.getValueLabel(value.value)
                    + " "
                    + card.getSuitLabel(value.suit)
                    + "</li>"
                );
            });
        } else {
            $("#playerStats_cards .playerStats_container").html("---");
        }

        if (Object.keys(items).length > 0) {
            $("#playerStats_items .playerStats_container").append('<ul></ul>');
            $.each(items, function (key, value) {
                $("#playerStats_items .playerStats_container ul").append(
                    "<li>"
                    + value.text
                    + "</li>"
                );
            });
        } else {
            $("#playerStats_items .playerStats_container").append("---");
        }

        if (Object.keys(upgrades).length > 0) {
            $("#playerStats_upgrades .playerStats_container").append('<ul></ul>');
            $.each(upgrades, function (key, value) {
                $("#playerStats_upgrades .playerStats_container ul").append(
                    "<li>"
                    + value.text
                    + "</li>"
                );
            });
        } else {
            $("#playerStats_upgrades .playerStats_container").append("---");
        }

        if (Object.keys(keywords).length > 0) {
            $("#playerStats_keywords .playerStats_container").append('<ul></ul>');
            $.each(keywords, function (key, value) {
                $("#playerStats_keywords .playerStats_container ul").append(
                    "<li>"
                    + value.text
                    + "</li>"
                );
            });
        } else {
            $("#playerStats_keywords .playerStats_container").append("---");
        }
    },
    showEffects : function () {
        $.each(effects, function (key, value) {
            if (value.condition()) {
                value.show();
            }
        });
    }
};


