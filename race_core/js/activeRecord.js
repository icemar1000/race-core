var AR ={
    dbPath : 'race.json',
    db : {},
    prepareRows : function (row) {
        if(!row){
            return [];
        }
        let arr = [];
        $.each(row.values, function (key, value) {
            arr[key] = AR.prepareOneRow(row, key);
        });
        return arr;
    },
    prepareOneRow : function (row, numberRow = 0) {
        if(!row){
            return {};
        }
        let obj = {};
        $.each(row.columns, function (key, value) {
            obj[value] = row.values[numberRow][key];
        });
        return obj;
    },
    getDb : function () {
        let xhr = new XMLHttpRequest();
        xhr.open('GET', this.dbPath, true);
        xhr.onload = function (e) {
            AR.db = JSON.parse(this.response);
            console.log(AR.db);
            view.initialize();
        };
        xhr.send();
    },
    getPageById : function (id) {
        let result = [];
        $.each(this.db.pages, function(key, value){
            if (value.id == id){
                result = value;
                return false;
            }
        });
        return result;
    },
    getAction : function (id) {
        let result = [];
        $.each(this.db.actions, function(key, value){
            if (value.id == id){
                result = value;
                return false;
            }
        });
        return result;  
    },
    getObjectByTitle : function (title){
        let result = [];
        $.each(this.db.objects,function(key, value){
            if (value.title == title){
                result = value;
                return false;
            }
        });
        return result;
    },
    getActions : function (pageId) {
        let result = this.db.actions.filter(function(action){
            return action.from_id == pageId;
        });
        return result;
    },
    getObjectsKeywords : function (){
        let result = this.db.objects.filter(function(object){
            return object.category == 'Ключевое слово';
        });
        return result;
    },
    getTriggersByPageOnes : function(id){
        let result = this.db.triggers.filter(function(trigger){
            return trigger.page_id == id && trigger.ones == 1;
        });
        return result;
    },
    getTriggersByPageMultiple : function(id){
        let result = this.db.triggers.filter(function(trigger){
            return trigger.page_id == id && trigger.ones == 0;
        });
        return result;
    },
    getPagesByTag : function (tag){
        let result = this.db.pages.filter(function(page){
            var approve = false;
            $.each(page.tags, function(key, value){
                if (value.title == tag){
                    approve = true;
                    return false;
                }
            });
            return approve;
        });
        return result; 
    }
};