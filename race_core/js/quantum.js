var quantum = {
    isShow : false,
    firstQuantumPage : {},
    secondQuantumPage : {},
    show : function(){
        if (this.isShow){
            this.getQuantum();
            this.showQuantum();
        } else {
            $("#first_page_quantum span").hide();
            $("#second_page_quantum span").hide();
        }
        return false;
    },
    getQuantum : function() {
        let pages = AR.getPagesByTag("Квантовый параграф");
        pages = this.excludePageById(pages, game.data.page.id);
        this.firstQuantumPage = pages[app.rand(0, pages.length - 1)];
        pages = this.excludePageById(pages, this.firstQuantumPage.id);
        this.secondQuantumPage = pages[app.rand(0, pages.length - 1)];
    },
    showQuantum : function(){
        $("#first_page_quantum span").html(this.firstQuantumPage.text);
        $("#second_page_quantum span").html(this.secondQuantumPage.text);
        $("#first_page_quantum span").show();
        $("#second_page_quantum span").show();
    },
    excludePageById : function(pages, id){
        let result = pages.filter(function(page){
            return page.id != id;
        });
        return result;
    }
};


