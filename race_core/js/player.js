var player = {
    game : {
        "pageId": 0,
        "data": "",
        "money": 0,
        "visitedPages" : []
    },
    gameObjects : {},
    cards : {},
    initialize : function () {
        $.each(card.getSuits(),function(key, suit) {
            for (let i = 0; i <= 4; i++) {
                player.cards.push({
                    "value"  : i,
                    "suit" : suit,
                    "place" : card.PLACE_DECK
                });
            }
        });
        player.cards.push({
            "value" : 5,
            "suit" : card.SUIT_JOKER,
            "place" : card.PLACE_JOKER
        });
        $.each(view.viewMarkers, function (key, marker) {
            player.addCounter(marker, 0); 
        });        
    },     
    setPage : function (id) {
        var event = new Event('page.set');
        document.dispatchEvent(event);
        this.game.pageId = id;        
    },
    getPage : function () {
        this.getData();
        return this.game.pageId;
    },
    visitPage : function (pageId) {
        this.game.visitedPages.push(pageId);
    },
    getData : function () {
        if (this.checkSave()) {
            let _player = JSON.parse(localStorage.getItem('player'));
            let _gameData = JSON.parse(localStorage.getItem('game'));
            this.game = _player.game;
            this.gameObjects = _player.gameObjects;
            this.cards = _player.cards;
            game.data = _gameData;
        }
    },
    saveData : function () {
        let jsonPlayer = JSON.stringify(this, null, ' ');
        let jsonGame = JSON.stringify(game.data, null, ' ');
        localStorage.setItem('player', jsonPlayer);
        localStorage.setItem('game', jsonGame);
    },
    clearData : function () {
        this.game = {
            "pageId": 0,
            "data": "",
            "money": 0,
            "visitedPages" : []
        };
        this.gameObjects = {}; 
        this.cards = [];
    },
    checkSave : function () {
        if(localStorage.getItem('player')) {
            return true;
        }
        return false;
    },
    changeMoney : function (value) {
        this.game.money = parseInt(this.game.money) + parseInt(value);
        if (value > 0) {
            game.addNotice("Зачислено " + value + " кредитов.");
        } else {
            game.addNotice("Списано " + (-value) + " кредитов.");
        }
        interface.showMoney();
    },
    addCounter : function (title, value) {
        let object = AR.getObjectByTitle(title);
        this.addObject(object);
        this.gameObjects[object.id].value+=parseInt(value);
        interface.showCounters();
    },
    addObject : function (object, value = 0) {
        if (!this.gameObjects[object.id]) {
            this.gameObjects[object.id] = {
              "value" : value,
              "title" : object.title,
              "text" : object.text,
              "type" : object.category
            }
        }
        switch (object.category) {
            case "Улучшение":
                game.addNotice("Установлен апгрейд " + "«" + object.text + "»");
                interface.showUpgradesList();
            break;
            case "Предмет":
                game.addNotice("Получен предмет " + "«" + object.text + "»");
                interface.showItemsList();
            break;
            case "Ключевое слово":
                game.addNotice("Получен чекпоинт " + "«" + object.text + "»");
                interface.showKeywordsList();
            break;
        }
        
    },
    removeObject : function (id) {
        game.addNotice("Потерян предмет " + "«" + this.gameObjects[id].text + "»")
        delete this.gameObjects[id];
        game.data.onlyEvents = false;
    },
    buyItem : function (title, value) {
        if (this.game.money>=value){
            this.addObject(AR.getObjectByTitle(title));
            this.changeMoney(value * (-1));
        }
    },
    getCards : function (place) {
        let result = {};
        $.each(this.cards, function (key, value) {
            if (value.place == place) {
                result[key] = value;
            }
        });
        return result;
    },
    getItems : function () {
        return this.getObjects("Предмет");
    },
    getKeywords : function () {
        return this.getObjects("Ключевое слово");
    },
    getUpgrades : function () {
        return this.getObjects("Улучшение");
    },
    getMarkers : function () {
        let markers = {};        
        $.each(this.gameObjects, function (key, object) {
            if (view.viewMarkers.indexOf(object.title) !== -1) {
               markers[key] = object;
            }
        });        
        return markers;
    },
    getObjects : function (type) {
        let objects = {};
        $.each(this.gameObjects, function (key, object) {
            if (object.type == type) {
               objects[key] = object;
            }
        });
        return objects;
    },
    discardCard : function (key) {
        this.cards[key].place = card.PLACE_DISCARD;
        game.data.onlyEvents = false;
        game.addNotice(
            "Сброшена карта "
            + "«"
            + card.getValueLabel(this.cards[key].value) + (card.getValueLabel(this.cards[key].value).length > 0 ? " " : "")
            + card.getSuitLabel(this.cards[key].suit)
            + "»"
        );
        interface.showCards();
    },
    drawCard : function (key) {
        this.cards[key].place = card.PLACE_HAND;
        game.data.onlyEvents = false;
        game.addNotice(
            "Получена карта "
            + "«"
            + card.getValueLabel(this.cards[key].value) + (card.getValueLabel(this.cards[key].value).length > 0 ? " " : "")
            + card.getSuitLabel(this.cards[key].suit)
            + "»"
        );
        interface.showCards();
    },
    getGameObjectIdByTitle (title) {
        let id = false;
        $.each(this.gameObjects, function (key, object) {
            if (object.title == title) {
               id = key;
            }
        });
        return id;
    }
};


//    "gameObjects" : {
//       id 1 : {
//            "value"  : 1,
//            "title" : "Хуевина",
//            "text" : "Великолепный девайс",
//            "type" : "Предмет"
//        }
//    }
//    
//    "cards" : {
//       id 1 : {
//            "value"  : 1,
//            "suit" : "hearts",
//            "place" : "deck"
//        }
//    }
//};