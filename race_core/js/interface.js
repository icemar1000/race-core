let clickHandled = false;
$('#toggleGameMenu').on({
    click: function () {
        if (!clickHandled) {
            clickHandled = true;
            if ($('body').hasClass('isGameMenuActive')) {
                $('#playerStats').animate({
                    width: "0"
                }, 0, function () {
                    $('body').toggleClass('isGameMenuActive');
                    clickHandled = false;
                });
            } else {
                $('body').toggleClass('isGameMenuActive').scrollTop(0);
                $('#playerStats').animate({
                    width: "100%"
                }, 0, function () {
                    clickHandled = false;
                });
            }
        }
    }
});
$('.tablet-footer-icon').on({
    click: function () {
        var screen= $(this).attr('data-screen');
        $('.tablet-footer-icon').removeClass('active');
        $('.tablet-footer-icon.icon-' + screen).addClass('active');
        $('.gameUI').removeClass('active');
        $('.gameUI.' + screen).addClass('active');
    }
});
var interface = {
    show: function () {
        this.showUpgradesList();
        this.showItemsList();
        this.showKeywordsList();
        this.showMoney();
        this.showCounters();
        this.showCards();
    },
    showMoney : function () {
        $('.money').html("Кредиты: " + player.game.money);
    },
    showUpgradesList : function(){
        $('.upgrades-list').html('');
        $.each(player.getUpgrades(), function (key, upgrade) {
            $('.upgrades-list').append('<li>'+upgrade.text+'</li>');
        });
    },
    showItemsList: function(){
        $('.items-list').html('');
        $.each(player.getItems(), function (key, item) {
            $('.items-list').append('<li>'+item.text+'</li>');
        });
    },
    showKeywordsList : function(){
        $('.keywords-list').html('');
        $.each(player.getKeywords(), function (key, keyword) {
                $('.keywords-list').append('<li>'+keyword.text+'</li>');
        });
    },
    showCounters : function(){
        markers = player.getMarkers();
        $.each(markers, function(key, marker){
            $('.'+ key + '-marker').css('width',(marker.value/0.2)+'%');
        }); 
    },
    showCards : function(){
        cards = player.getCards('hand');
        $('.cards-list').html('');
        var values = {
            0 :"10",
            1 :"J",
            2 :"Q",
            3 :"K",
            4 :"A",
            5 :""
        };
        var suits = {
            "hearts" : '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" ><path d="M0 0h512v512H0z" fill="transparent" fill-opacity="0"></path><g class="" transform="translate(0,0)" style="touch-action: none;"><path d="M480.25 156.355c0 161.24-224.25 324.43-224.25 324.43S31.75 317.595 31.75 156.355c0-91.41 70.63-125.13 107.77-125.13 77.65 0 116.48 65.72 116.48 65.72s38.83-65.73 116.48-65.73c37.14.01 107.77 33.72 107.77 125.14z" fill="#48baff" fill-opacity="1"></path></g></svg>',
            "diamonds" : '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M0 0h512v512H0z" fill="transparent" fill-opacity="0"></path><g class="" transform="translate(0,0)" style="touch-action: none;"><path d="M431.76 256c-69 42.24-137.27 126.89-175.76 224.78C217.51 382.89 149.25 298.24 80.24 256c69-42.24 137.27-126.89 175.76-224.78C294.49 129.11 362.75 213.76 431.76 256z" fill="#48baff" fill-opacity="1"></path></g></svg>',
            "clubs" : '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M0 0h512v512H0z" fill="transparent" fill-opacity="0"></path><g class="" transform="translate(0,0)" style="touch-action: none;"><path d="M477.443 295.143a104.45 104.45 0 0 1-202.26 36.67c-.08 68.73 4.33 114.46 69.55 149h-177.57c65.22-34.53 69.63-80.25 69.55-149a104.41 104.41 0 1 1-66.34-136.28 104.45 104.45 0 1 1 171.14 0 104.5 104.5 0 0 1 135.93 99.61z" fill="#48baff" fill-opacity="1"></path></g></svg>',
            "spades" : '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M0 0h512v512H0z" fill="transparent" fill-opacity="0"></path><g class="" transform="translate(0,0)" style="touch-action: none;"><path d="M458.915 307.705c0 62.63-54 91.32-91.34 91.34-41.64 0-73.1-18.86-91.83-34.26 2.47 50.95 14.53 87.35 68.65 116h-176.79c54.12-28.65 66.18-65.05 68.65-116-18.73 15.39-50.2 34.28-91.83 34.26-37.29 0-91.34-28.71-91.34-91.34 0-114.47 80.64-83.32 202.91-276.49 122.28 193.17 202.92 162.03 202.92 276.49z" fill="#48baff" fill-opacity="1"></path></g></svg>',
            "joker" : '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M0 0h512v512H0z" fill="transparent" fill-opacity="0"></path><g class="" transform="translate(0,0)" style="touch-action: none;"><path d="M119.436 36c-16.126 0-29.2 17.237-29.2 38.5v363c0 21.263 13.074 38.5 29.2 38.5h275.298c16.126 0 29.198-17.237 29.198-38.5v-363c0-21.263-13.072-38.5-29.198-38.5zm26.369 10.951l11.002 32.856 34.648.312-27.848 20.617 10.41 33.05-28.212-20.114-28.215 20.113L128 100.736 100.152 80.12l34.649-.312zM363.979 161.84c7.127 9.459 12.739 20.689 16.832 32.04 3.8 10.544 6.197 21.211 6.668 31.02-.163 19.015-3.915 23.274-14.557 36.934l-6.703-11.48c-10.85-13.106-30.779-48.4-47.383-43.672-6.521 6.11-8.996 13.37-10.313 20.802 2.898 8.8 4.477 18.43 4.477 28.516 0 15.293-3.615 29.54-9.996 41.416 22.643 4.537 57.927 19.332 57.973 39.223-.27 3.783-1.835 7.68-4.362 10.42-10.743 12.528-36.958 4.125-45.2 10.072.796 6.947 4.112 14.118 4.355 20.174.136 4.36-1.768 10.58-6.508 13.996-5.67 4.087-12.968 4.551-18.52 3.045C279.94 392.226 272 379.649 256 377c-13.544 3.491-22.412 13.87-34.742 17.346-5.552 1.506-12.85 1.042-18.52-3.045-4.74-3.417-6.644-9.636-6.508-13.996-.058-7.142 4.107-13.794 4.356-20.174-15.741-7.788-33.816 1.97-45.201-10.072-2.527-2.74-4.093-6.637-4.362-10.42 6.146-27.341 35.374-34.684 57.973-39.223C202.615 285.54 199 271.293 199 256c0-11.489 2.047-22.385 5.764-32.135-2.357-7.923-3.441-15.988-9.438-22.441-8.758-.925-14.079 6.897-17.842 12.63-11.683 19.5-18.718 30.606-32.88 46.192-16.604-23.4-19.314-49.29-13.157-70.988 6.065-20.331 19.17-38.798 37.926-47.924 21.216-9.766 39.872-10.03 58.885.203 5.163-13.053 10.4-25.65 18.035-36.209 9.625-13.31 23.8-25.631 43.707-25.295 38.8.656 73.993 51.156 73.979 81.807zm-72.22-63.893c-35.759 2.409-44.771 44.746-55.189 71.29l-9.447-7.087c-18.428-12.31-31.076-13.732-49.875-4.63-12.924 6.288-23.701 20.62-28.553 36.882-3.38 11.329-3.765 23.225-.949 33.645 9.45-13.549 15.806-30.08 28.317-39.178 7.486-7.975 26.27-8.498 35.45 3.897 4.838 7.02 7.437 14.54 9.5 22.234h72.165c.592-1.944 1.067-3.762 2.017-6.033 2.956-7.064 7.765-16.266 18.395-19.504 18.09-3.862 32.494 7.106 43.498 18.514 4.517 4.717 8.492 9.696 12.098 14.517-.69-6.798-2.477-14.651-5.31-22.508-13.127-36.707-37.889-51.031-70.386-32.011 2.556-16.423 16.87-35.72 46.25-26.962-9.094-17.135-30.355-42.471-47.98-43.066zM220.644 233c-2.31 6.965-3.643 14.753-3.643 23 0 15.85 4.892 30.032 12.26 39.855C236.628 305.68 245.988 311 256 311c10.012 0 19.372-5.32 26.74-15.145C290.108 286.032 295 271.85 295 256c0-8.247-1.334-16.035-3.643-23zM232 280h48s-8 14-24 14-24-14-24-14zm-11.14 33.566c-13.86 3.34-50.369 8.9-51.842 21.42 9.621 1.947 20.446.838 28.998 2.235 5.993 1.018 12.82 3.323 17.285 9.517 3.375 4.683 3.577 10.103 3.037 14.21-.543 5.89-3.317 10.557-3.975 16.32 15.955-2.59 28.264-17.532 41.637-18.268 16-.702 29.313 17.402 41.637 18.268-.893-5.59-3.262-11.158-3.975-16.32-.54-4.107-.338-9.527 3.037-14.21 4.465-6.194 11.292-8.5 17.285-9.517 9.742-2.229 19.975.396 28.998-2.235-5.77-13.125-39.813-19.454-51.841-21.42C281.665 323.01 269.45 329 256 329c-13.452 0-25.665-5.991-35.14-15.434zm117.122 64.649l28.213 20.113 28.215-20.113L384 411.264l27.848 20.617-34.649.312-11.004 32.856-11.002-32.856-34.648-.312 27.848-20.617z" fill="#48baff" fill-opacity="1"></path></g></svg>'
        };
        $.each(cards, function(key, card){
            $('.cards-list').append('<div class="cardWrap col-auto">'+
                                        '<div class="cardInner">'+
                                        '   <div class="cardHeader">'+
                                        '        <span class="cardValue">' + values[card.value] + '</span>'+
                                        '        <span class="icon-card icon-card-hearts">'+
                                        suits[card.suit] +
                                        '        </span>'+
                                        '    </div>'+
                                        '    <div class="cardBody">'+
                                        '        <span class="icon-card icon-card-hearts">'+
                                        suits[card.suit] +
                                        '        </span>'+
                                        '    </div>'+
                                        '    <div class="cardFooter">'+
                                        '        <span class="cardValue">' + values[card.value] + '</span>'+
                                        '        <span class="icon-card icon-card-hearts">'+
                                        suits[card.suit] +
                                        '        </span>'+
                                        '    </div>'+
                                        '</div>'+
                                    '</div>');
        }); 
    }
}