var music = {
    status : '',
    file : '',
    newFile : '',
    tagAmbient : {
        'Тест музыки' : 'music/test1.mp3',
        'Тест музыки 2' : 'music/test2.mp3'
    },
    call : function(){
        let isSoundPage = false;
        $.each(game.data.page.tags, function(key, tag){
            if (tag.title in music.tagAmbient){
                isSoundPage = true;
                if (music.status != tag.title){
                    music.start(tag.title);
                    music.status = tag.title;
                }    
            }
        });
        if (!isSoundPage){
            this.stop();
            this.status = '';
        }
    },
    stop : function(){
        if (this.file){
            this.file.fade(1.0, 0.0, 1500);
            this.file.once('fade', function() {
                music.file.stop();
            });
        }
    },
    start : function(src){
        if (this.file){
            this.changeSound(src);
        } else {
            this.file = new Howl({
                src: [music.tagAmbient[src]],
                loop: true
            });
            this.file.play();
            this.file.fade(0.0, 1.0, 1500);
        }
    },
    changeSound : function(src){
        this.file.fade(1.0, 0.0, 1500);
        this.newFile = new Howl({
            src: [music.tagAmbient[src]],
            loop: true
        });
        this.newFile.play();
        this.newFile.fade(0.0, 1.0, 1500);
        this.file.once('fade', function() {
            music.file.stop();
            music.file = music.newFile;
        });
    }
};
    