var canvas = {
    canvas: document.getElementById('space'),
    gl: '',
    selectProgram : '0',
    programs: [],
    NUM_POINTS: 20000,//количество звезд
    points: [],
    buffer: '',
    pMatrix: mat4.create(),
    vMatrix: mat4.create(),
    ivMatrix: mat4.create(),
    mMatrix: mat4.create(),
    mvMatrix: mat4.create(),
    mvpMatrix: mat4.create(),
    position: vec3.create(),
    angle: 0.0,
    angleChange: 0.0005,
    uniforms:[],
    attributes: [],
    stop: false,
    size: 4.0,
    maximize: false,
    
    createFirstProgram : function(){
        let vertex = this.gl.createShader(this.gl.VERTEX_SHADER);
        let fragment = this.gl.createShader(this.gl.FRAGMENT_SHADER);
        this.programs[0] = this.gl.createProgram();
        this.buffer = this.gl.createBuffer();
        
        this.gl.shaderSource(vertex, `
        precision mediump float;

        uniform mat4 mvp;

        attribute vec3 position;
        
        uniform float size;
        
        varying float v_positionWithOffset;
        
        void main(void) {
          vec4 finalPosition = mvp * vec4(position, 1.0);

          gl_Position = finalPosition;

          if (gl_Position.w > 0.0) {
            gl_PointSize = size / gl_Position.w;
          } else {
            gl_PointSize = 0.0;
          }

        }`);
    
        this.gl.compileShader(vertex);
        if (!this.gl.getShaderParameter(vertex, this.gl.COMPILE_STATUS)) {
          console.error(this.gl.getShaderInfoLog(vertex));
        }
        //В константах begin и end - цвет звезд
        this.gl.shaderSource(fragment, `
        precision highp float;
        varying float v_positionWithOffset;
        const vec4 begin = vec4(0.1, 0.75, 1.0, 1.0); 
        const vec4 end = vec4(1.0, 1.0, 1.0, 1.0);

        vec4 interpolate4f(vec4 a,vec4 b, float p) {
          return a + (b - a) * p;
        }

        void main(void) {

          vec2 pc = (gl_PointCoord - 0.5) * 2.0;

          float dist = (1.0 - sqrt(pc.x * pc.x + pc.y * pc.y));
          
          vec4 color = interpolate4f(begin, end, dist);

          gl_FragColor = vec4(dist, dist, dist, dist) * color;

        }`);
        this.gl.compileShader(fragment);
        if (!this.gl.getShaderParameter(fragment, this.gl.COMPILE_STATUS)) {
          console.error(this.gl.getShaderInfoLog(fragment));
        }

        this.gl.attachShader(this.programs[0], vertex);
        this.gl.attachShader(this.programs[0], fragment);
        this.gl.linkProgram(this.programs[0]);
        
        this.uniforms[0] =  {
          mvp: this.gl.getUniformLocation(this.programs[0], "mvp"),
          size: this.gl.getUniformLocation(this.programs[0], "size")
        };
        this.attributes[0] = {
          position: this.gl.getAttribLocation(this.programs[0], "position"),
        };
    },
    createRedProgram: function(){
        this.programs[1] = this.gl.createProgram();
        this.buffer = this.gl.createBuffer();
        let vertex = this.gl.createShader(this.gl.VERTEX_SHADER);
        let fragment = this.gl.createShader(this.gl.FRAGMENT_SHADER);
        this.gl.shaderSource(vertex, `
        precision mediump float;

        uniform mat4 mvp;

        attribute vec3 position;

        varying float v_positionWithOffset;
        
        void main(void) {
          vec4 finalPosition = mvp * vec4(position, 1.0);

          gl_Position = finalPosition;

          if (gl_Position.w > 0.0) {
            gl_PointSize = 4.0 / gl_Position.w;
          } else {
            gl_PointSize = 0.0;
          }
          v_positionWithOffset = fract(sin(dot(position.xy ,vec2(12.9898,78.233))) * 43758.5453);
        }`);
    
        this.gl.compileShader(vertex);
        if (!this.gl.getShaderParameter(vertex, this.gl.COMPILE_STATUS)) {
          console.error(this.gl.getShaderInfoLog(vertex));
        }
        //В константах begin и end - цвет звезд
        this.gl.shaderSource(fragment, `
        precision highp float;
        varying float v_positionWithOffset;
        const vec4 begin = vec4(0.1, 0.75, 1.0, 1.0); 
        const vec4 end = vec4(1.0, 1.0, 1.0, 1.0);

        vec4 interpolate4f(vec4 a,vec4 b, float p) {
          return a + (b - a) * p;
        }

        void main(void) {

          vec2 pc = (gl_PointCoord - 0.5) * 2.0;

          float dist = (1.0 - sqrt(pc.x * pc.x + pc.y * pc.y));
          
          vec4 color = interpolate4f(begin, end, dist);
          //связь
          if (v_positionWithOffset > 0.7){
            color = vec4(1.0, 0.0, 0.0, 1.0);
          }
          gl_FragColor = vec4(dist, dist, dist, dist) * color;

        }`);
        this.gl.compileShader(fragment);
        if (!this.gl.getShaderParameter(fragment, this.gl.COMPILE_STATUS)) {
          console.error(this.gl.getShaderInfoLog(fragment));
        }

        this.gl.attachShader(this.programs[1], vertex);
        this.gl.attachShader(this.programs[1], fragment);
        this.gl.linkProgram(this.programs[1]);

        canvas.uniforms[1] =  {
          mvp: this.gl.getUniformLocation(this.programs[1], "mvp")
        };
        canvas.attributes[1] = {
          position: this.gl.getAttribLocation(this.programs[1], "position")
        };
    },
    createLightProgram: function(){
        let vertex = this.gl.createShader(this.gl.VERTEX_SHADER);
        let fragment = this.gl.createShader(this.gl.FRAGMENT_SHADER);
        this.programs[2] = this.gl.createProgram();
        this.buffer = this.gl.createBuffer();
        
        this.gl.shaderSource(vertex, `
        precision mediump float;

        uniform mat4 mvp;

        attribute vec3 position;

        varying float v_positionWithOffset;
        
        void main(void) {
          vec4 finalPosition = mvp * vec4(position, 1.0);

          gl_Position = finalPosition;

          if (gl_Position.w > 0.0) {
            gl_PointSize = 15.0 / gl_Position.w;
          } else {
            gl_PointSize = 0.0;
          }

        }`);
    
        this.gl.compileShader(vertex);
        if (!this.gl.getShaderParameter(vertex, this.gl.COMPILE_STATUS)) {
          console.error(this.gl.getShaderInfoLog(vertex));
        }
        //В константах begin и end - цвет звезд
        this.gl.shaderSource(fragment, `
        precision highp float;
        varying float v_positionWithOffset;
        const vec4 begin = vec4(0.5, 1.0, 1.0, 0.0); 
        const vec4 end = vec4(0.5, 1.0, 1.0, 1.0);

        vec4 interpolate4f(vec4 a,vec4 b, float p) {
          return a + (b - a) * p;
        }

        void main(void) {

          vec2 pc = (gl_PointCoord - 0.5) * 2.0;

          float dist = (1.0 - sqrt(pc.x * pc.x + pc.y * pc.y));
          
          vec4 color = interpolate4f(begin, end, dist);

          gl_FragColor = vec4(dist, dist, dist, dist) * color;

        }`);
        this.gl.compileShader(fragment);
        if (!this.gl.getShaderParameter(fragment, this.gl.COMPILE_STATUS)) {
          console.error(this.gl.getShaderInfoLog(fragment));
        }

        this.gl.attachShader(this.programs[2], vertex);
        this.gl.attachShader(this.programs[2], fragment);
        this.gl.linkProgram(this.programs[2]);
        
        this.uniforms[2] =  {
          mvp: this.gl.getUniformLocation(this.programs[2], "mvp")
        };
        this.attributes[2] = {
          position: this.gl.getAttribLocation(this.programs[2], "position")
        };
    },
    initialize: function(){
        this.gl = this.canvas.getContext("webgl", {
            antialias: false,
            alpha: false,
            premultipliedAlpha: false
        });
        this.vertexShader = this.gl.createShader(this.gl.VERTEX_SHADER);
        this.fragmentShader = this.gl.createShader(this.gl.FRAGMENT_SHADER);
        
        this.createFirstProgram();
        this.createRedProgram();
        this.createLightProgram();
        
        const attributes = this.attributes;
        const uniforms = this.uniforms;

        for (let index = 0; index < this.NUM_POINTS; index++) {
          this.points.push((Math.random() - 0.5) * 8);
          this.points.push((Math.random() - 0.5) * 8);
          this.points.push((Math.random() - 0.5) * 8);
        }

        this.gl.bindBuffer(this.gl.ARRAY_BUFFER, this.buffer);
        this.gl.bufferData(this.gl.ARRAY_BUFFER, new Float32Array(this.points), this.gl.STATIC_DRAW);

        this.gl.enable(this.gl.BLEND);
        this.gl.blendFunc(this.gl.SRC_ALPHA, this.gl.ONE);

        mat4.perspective(this.pMatrix, Math.PI * 0.35, this.canvas.width / this.canvas.height, 0.01, 100000.0);

        vec3.set(this.position,0.0,0.0,0.0);

        canvas.render();
        this.resize();
        window.addEventListener("resize", this.resize);
    },
    render: function(now){
        canvas.gl.clearColor(0.0, 0.0, 0.0, 1.0);//Цвет космоса
        canvas.gl.clear(canvas.gl.COLOR_BUFFER_BIT);

        canvas.gl.viewport(0,0,canvas.canvas.width,canvas.canvas.height);

        // P * V * M
        //mat4.translate(mvpMatrix, mvpMatrix, position);
        mat4.identity(canvas.mMatrix);
        if (!canvas.stop){
            canvas.angle += canvas.angleChange;
            canvas.position[2] = Math.sin(now / 50000);
        }
        if (canvas.maximize && canvas.size < 350){
            canvas.size += 1;
        }

        mat4.identity(canvas.vMatrix);
        mat4.translate(canvas.vMatrix, canvas.vMatrix, canvas.position);
        mat4.rotateX(canvas.vMatrix, canvas.vMatrix, canvas.angle);
        mat4.rotateY(canvas.vMatrix, canvas.vMatrix, canvas.angle);
        mat4.rotateZ(canvas.vMatrix, canvas.vMatrix, canvas.angle);

        mat4.invert(canvas.ivMatrix, canvas.vMatrix);

        mat4.multiply(canvas.mvMatrix, canvas.ivMatrix, canvas.mMatrix);
        mat4.multiply(canvas.mvpMatrix, canvas.pMatrix, canvas.mvMatrix);

        canvas.gl.useProgram(canvas.programs[canvas.selectProgram]);
        canvas.gl.enableVertexAttribArray(canvas.attributes[canvas.selectProgram].position);
        canvas.gl.uniformMatrix4fv(canvas.uniforms[canvas.selectProgram].mvp, false, canvas.mvpMatrix);
        canvas.gl.uniform1f(canvas.uniforms[canvas.selectProgram].size, canvas.size);
        canvas.gl.vertexAttribPointer(0, 3, canvas.gl.FLOAT, canvas.gl.FALSE, 3*4, 0);
        canvas.gl.drawArrays(canvas.gl.POINTS, 0, canvas.NUM_POINTS);
        
        //console.count("render");

        window.requestAnimationFrame(canvas.render);
    },
    resize: function(){
        this.canvas.width = window.innerWidth;
        this.canvas.height = window.innerHeight;

        mat4.perspective(this.pMatrix, Math.PI * 0.35, this.canvas.width / this.canvas.height, 0.01, 1000.0);
    },
};
canvas.initialize();



