var visual = {
    status : '',
    tagBackgrounds : {
        "затемнение" : function(){
            $('body').addClass('darkness');
        },
        "флешбек" : function(){
            $('body').addClass('blur');
        },
        "разговор с Тао" : function(key){
            $("#actions [data-key='" + key + "']").addClass("tao");
        },
        "красные звёзды" : function(){
            canvas.selectProgram = 1;
        },
        "неоновые звёзды" : function(){
            canvas.selectProgram = 2;
        },
        "вращение в другую сторону" : function(){
            canvas.angleChange = -canvas.angleChange;
        },
        "остановите космос" : function(){
            canvas.stop = true;
        },
        "Квантовый параграф" : function(){
            quantum.isShow = true;
        }
    },
    call : function(){
        let isVisualPage = false;
        $.each(game.data.page.tags, function(key, tag){
            if (tag.title in visual.tagBackgrounds){
                isVisualPage = true;
                if (visual.status != tag.title){
                    $('body').removeClass();
                    visual.status = tag.title;
                    console.log(visual.status);
                    visual.tagBackgrounds[tag.title]();
                }    
            }
        });
        $.each(game.data.actions, function(key, action){
            $.each(action.tags, function(tag_key, tag){
                visual.tagBackgrounds[tag.title](key);
            });
        });
        if (!isVisualPage){
            $('body').removeClass();
            canvas.selectProgram = 0;
            canvas.angleChange = 0.0005;
            canvas.stop = false;
            this.status = '';
            quantum.isShow = false;
        }
    },
};