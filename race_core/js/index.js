game.initialize();
$('body').on("click", ".link.action:not(.manual):not(.disable)", function () {
    game.move($(this).attr("data-id"));
});
$('body').on("click", ".link.action.manual:not(.disable)", function () {
    trigger.execute($(this).attr("data-event"));
    game.show();
});
$('#showPage').on("click", function () {
    let pageNumber = $("#pageNumber").val();
    if (pageNumber) {
        app.goPage(pageNumber);
    }
});
document.addEventListener("page.set", function () {
    $('.pageFixedWrap').scrollTop(0);
    window.scrollTo(0, 0);
});